﻿Public Class Form1
    'ストップウォッチ
    Dim stpW As Diagnostics.Stopwatch = New Diagnostics.Stopwatch()
    Dim lastStpWTime As TimeSpan = New TimeSpan()

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "StopWatch"
        lblDisplay.Text = String.Empty
        Timer1.Interval = 100 '0.1sec
        Timer1.Enabled = False
    End Sub

    'start button
    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        stpW.Start()
        Timer1.Enabled = True
        btnReset.Enabled = False
    End Sub
    'stop button
    Private Sub btmStop_Click(sender As Object, e As EventArgs) Handles btmStop.Click
        stpW.Stop()
        Timer1.Enabled = False
        btnReset.Enabled = True
    End Sub
    'reset button
    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        stpW.Reset()
        lblDisplay.Text = stpW.Elapsed.ToString("hh\:mm\:ss\.f")
        lbxLap.Items.Clear()
        lbxSplit.Items.Clear()
        Timer1.Enabled = False

    End Sub
    'timer
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        lblDisplay.Text = stpW.Elapsed.ToString("hh\:mm\:ss\.f")
    End Sub
    'split button
    Private Sub btnSplit_Click(sender As Object, e As EventArgs) Handles btnSplit.Click
        If stpW.IsRunning Then
            lbxSplit.Items.Insert(0, stpW.Elapsed.ToString("hh\:mm\:ss\.f"))
        End If
    End Sub
    'lap button
    Private Sub btnLap_Click(sender As Object, e As EventArgs) Handles btnLap.Click

        If stpW.IsRunning Then

            Dim currentTime As TimeSpan = stpW.Elapsed
            lbxLap.Items.Insert(0, (currentTime - lastStpWTime).ToString("hh\:mm\:ss\.f"))
            lastStpWTime = stpW.Elapsed

        End If
    End Sub
End Class
